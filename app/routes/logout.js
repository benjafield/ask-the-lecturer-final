import Ember from 'ember';

export default Ember.Route.extend({

	/**
	 * Perform logout actions and return to login page.
	 */
	beforeModel: function () {
		sessionStorage.removeItem('user_id');
		sessionStorage.removeItem('userdata');
		sessionStorage.removeItem('user_logged_in');
		window.location.href='/';
	}

});
