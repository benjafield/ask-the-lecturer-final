import Ember from 'ember';

export default Ember.Route.extend({

	/**
	 * Redirect if not logged in.
	 */
	beforeModel: function () {
		if (sessionStorage.getItem('user_logged_in') === null) this.transitionTo('home');
	},

	/**
	 * Setup controller to work with login.
	 */
	setupController: function (controller, model) {
		controller.set('model', model);
		if (sessionStorage.getItem('user_logged_in') !== null) controller.set('logged_in', true);
	},

	/**
	 * Load the lecture data into the model
	 * attribute on the route.
	 *
	 * Parameters
	 *
	 * lecture -> the lecture to find
	 */
	model: function (lecture) {

		/**
		 * Run the find query on the lecture model.
		 * Return the data to the route/view.
		 */
		return this.store.find('lecture', lecture.id);
		
	}

});
