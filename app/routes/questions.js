import Ember from 'ember';

export default Ember.Route.extend({

	/**
	 * Set up the model to return the question
	 * data.
	 */
	model : function () {

		/**
		 * Get the user ID from session storage and
		 * parse it as an integer.
		 */
		var user_id = parseInt(sessionStorage.user_id);

		return this.store.query('question', { owner: user_id });

	}

});
