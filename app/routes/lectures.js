import Ember from 'ember';

export default Ember.Route.extend({

	/**
	 * Redirect if not logged in.
	 */
	beforeModel: function () {
		if (sessionStorage.getItem('user_logged_in') === null) this.transitionTo('home');
	},

	/**
	 * Setup controller to work with login.
	 */
	setupController: function (controller, model) {
		controller.set('model', model);
		if (sessionStorage.getItem('user_logged_in') !== null) controller.set('logged_in', true);
	},

	/**
	 * Fill the model with lecture data.
	 */
	model: function () {

		/**
		 * Query the lectures from using the lecture model
		 * and return the results to the view.
		 *
		 * Pass the model name, followed by the query 
		 * object. In this case it is empty.
		 */
		return this.store.query('lecture', {});
		
	}

});