import Ember from 'ember';

export default Ember.Route.extend({

	/**
	 * Setup controller to work with login.
	 */
	setupController: function (controller, model) {
		controller.set('model', model);
		if (sessionStorage.getItem('user_logged_in') !== null) controller.set('logged_in', true);
	}

});
