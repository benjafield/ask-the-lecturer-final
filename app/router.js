import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  /**
   * Home route
   *
   * Where the login and registration will take place.
   */
  this.route('home', {
      path: '/'
  });

  /**
   * Lectures route
   *
   * Will handle requests to lectures.
   */
  this.route('lectures', function() {

      /**
       * Create
       *
       * This is where the create lecture form will be.
       */
      this.route('create');

      /**
       * View
       *
       * This is where an individual lecture and its
       * questions will be shown.
       * 
       * The ID of the lecture will be supplied
       * in order to determine which lecture
       * to show.
       */
      this.route('view', {
          path: '/:id'
      });

  });

  /**
   * Questions
   *
   * This is where the user will be able to view questions they have asked.
   */
  this.route('questions');

  this.route('logout');
});

export default Router;
