import DS from 'ember-data';

export default DS.RESTAdapter.extend({

	/**
	 * Host
	 *
	 * This is the URL of the API Host
	 */
	host: 'http://teaching.room3b.eu/ask-the-lecturer',

	/**
	 * Namespace
	 *
	 * This is the namespace of the application.
	 * For this it is 'api' so the full URL
	 * would be:
	 * 
	 * http://teaching.room3b.eu/ask-the-lecturer/api
	 */
	namespace: 'api',

	/**
	 * Headers
	 *
	 * Set the headers up in order to authenticate
	 * with the API. If authentication fails,
	 * no data can be added to the API.
	 */
	headers : function() {

		/**
		 * Auth string to pass as the header
		 * in USERID:PASSWORD format as 
		 * stated by the API.
		 */
		if (sessionStorage.getItem('user_logged_in') !== null) {

			var auth = parseInt(sessionStorage.getItem('user_id')) + ':' + JSON.parse(sessionStorage.getItem('userdata')).password;

			/**
			 * Return the headers object.
			 */
			return {
				'X-Atl-Authentication' : auth
			};

		}

	}.property().volatile()

});
