import DS from 'ember-data';

export default DS.Model.extend({

	/**
	 * The Lecture Model
	 *
	 * Properties
	 *
	 * Owner (number): the ID of the owner
	 * Title (string): the title of the lecture
	 * Date (string): the date of the lecture
	 * Questions (relationship): the questions related to the lecture
	 */
	owner: DS.attr(),
	title: DS.attr('string'),
	date: DS.attr('string'),
	questions: DS.hasMany('question', { async: true })
  
});
