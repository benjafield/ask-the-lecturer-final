import DS from 'ember-data';

export default DS.Model.extend({

	/**
	 * The Question Model
	 *
	 * Properties
	 *
	 * Owner (number): the ID of the owner user
	 * Question (string): the question text
	 * Answered (boolean): whether the question has been answered or not
	 * Lecture (relationship): link to the related lecture
	 */
	owner: DS.attr(),
	question: DS.attr('string'),
	answered: DS.attr('boolean'),
	lecture: DS.belongsTo('lecture', { async: true })
  
});
