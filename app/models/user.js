import DS from 'ember-data';

export default DS.Model.extend({
  
  	/**
  	 * Properties for the user records.
  	 */
	display_name: DS.attr('string'),
	email: DS.attr('string'),
	password: DS.attr('string'),
	lectures: DS.hasMany('lecture', { async: true })

});
