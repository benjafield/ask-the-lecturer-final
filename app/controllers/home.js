import Ember from 'ember';

export default Ember.Controller.extend({

	/**
	 * Home Controller
	 *
	 * Handles the login actions
	 */

	/**
	 * Login Error Properties
	 */
	login_error: false,
	login_message: '',

	/**
	 * Register Error Properties
	 */
	reg_error: false,
	reg_message : '',

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Logs the user in using jQuery AJAX
		 */
		login: function () {

			/**
			 * Move "this" to "controller"
			 */
			var controller = this;

			/**
			 * Get the email address and password from the form.
			 */
			var email = this.get('login_email'), password = this.get('login_password');

			/**
			 * Set errors and return false if no data has been supplied.
			 */
			if (email === undefined || password === undefined) {
				this.set('login_error', true);
				this.set('login_message', 'Please enter a username and password.');
				return false;
			}

			/**
			 * Run AJAX request to process the login.
			 */
			$.ajax({
				type: 'POST',
				url: 'http://teaching.room3b.eu/ask-the-lecturer/api/users/login',
				dataType: 'json',
				data: {
					email: email,
					password: password
				},
				success: function (r) {

					/**
					 * Set session storage with logged in user details.
					 */
					r.user.password = password;
					var userdata = JSON.stringify(r.user);
					sessionStorage.setItem('user_logged_in', '1');
					sessionStorage.setItem('user_id', r.user.id);
					sessionStorage.setItem('userdata', userdata);
					window.location.href="/lectures";

				},
				error: function (r) {

					var r = r.responseJSON.status;

					/**
					 * Make sure the controller knows there are errors
					 */
					controller.set('login_error', true);

					/**
					 * Find the errors and change the login_message value
					 * accordingly.
					 */
					for (var key in r.messages) {
						if (r.messages.hasOwnProperty(key)) {
					    	controller.set('login_message', r.messages[key]);
						}
					}
					
				}
			});

		},

		/**
		 * Register Action using jQuery AJAX
		 */
		register : function () {

			/**
			 * Get input values from form
			 */
			var display_name = this.get('reg_display_name'), email = this.get('reg_email'), password = this.get('reg_password'), confirm = this.get('reg_confirm');

			/**
			 * Validate the data.
			 */
			if (display_name === undefined || email === undefined || password === undefined) {
				this.set('reg_error', true);
				this.set('reg_message', 'Please fill out all required fields.');
				return false;
			}

			/**
			 * Confirm passwords are correct.
			 */
			if (password !== confirm) {
				this.set('reg_error', true);
				this.set('reg_message', 'The passwords do not match.');
				return false;
			}

			/**
			 * Create the user record
			 */ 
			var user = this.store.createRecord('user', {
				display_name: display_name,
				email: email,
				password: password
			});

			/**
			 * Store the "this" instance in controller to access within the
			 * "then" function.
			 */
			var controller = this;

			/**
			 * Save the record in storage, 
			 * then refresh the target.router, then transition
			 * to the new lecture's route.
			 */
			user.save().then(function() {
				controller.get('target.router').refresh();
				controller.transitionToRoute('home');
			});



		}

	}

});
