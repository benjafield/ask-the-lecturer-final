import Ember from 'ember';

export default Ember.Controller.extend({

	/**
	 * Actions for the Questions controller.
	 */
	actions: {

		/**
		 * Go To Lecture
		 *
		 * Redirects the user to the relevant lecture.
		 */
		go_to_lecture: function (lecture) {

			/**
			 * Make the transition
			 */
			this.transitionTo('lectures.view', lecture);

		}


	}

});
