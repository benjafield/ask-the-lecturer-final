import Ember from 'ember';

export default Ember.Controller.extend({

	/**
	 * The lectures/view (lectures/view.hbs) Controller
	 *
	 * Handle actions within the view.hbs file
	 */

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Save Question
		 *
		 * Add a question record and then save the 
		 * new record to storage.
		 * 
		 * Parameters
		 *
		 * lecture -> the lecture object the question belongs to.
		 */
		save_question: function (lecture) {

			/**
			 * Get the question text input value
			 */
			var question_text = this.get('question_text');

			/**
			 * Create the question record
			 */
			var question = this.store.createRecord('question', {
				question: question_text,
				lecture: lecture,
				owner: parseInt(sessionStorage.getItem('user_id')),
			});

			/**
			 * Store "this" in controller variable to use later.
			 */
			var controller = this;

			/**
			 * Save the record in storage (server) and refresh.
			 */
			question.save().then(function() {
				controller.get('target.router').refresh();
				controller.transitionTo('lectures.view', lecture);
			});

		},

		/**
		 * Toggle a question answered to unanswered
		 * & vice versa.
		 */
		toggle: function (question) {

			var answered = question.get('answered');
			answered = !answered;
			question.set('answered', answered);
			question.save();

		}

	}

});
