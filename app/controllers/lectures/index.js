import Ember from 'ember';

export default Ember.Controller.extend({

	/**
	 * The lectures/index (lectures/index.hbs) controller
	 *
	 * Handle actions within the index.hbs file
	 */

	logged_in: true,

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Go To Create Lecture
		 *
		 * Redirects the user to the create lecture route.
		 */
		go_to_create_lecture: function () {

			/**
			 * Transition to the appropriate route
			 */
			this.transitionTo('lectures.create');

		},

		/**
		 * Go To Lecture
		 *
		 * Redirects the user to the chosen lecture.
		 *
		 * Parameters
		 *
		 * lecture -> the lecture model in order to determine where to
		 *            transition to.
		 */
		go_to_lecture: function (lecture) {

			/**
			 * Transition to the lecture route.
			 */
			this.transitionTo('lectures.view', lecture);

		}

	}

});