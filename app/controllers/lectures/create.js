import Ember from 'ember';

export default Ember.Controller.extend({

	/**
	 * The lectures/create (lectures/create.hbs) Controller
	 *
	 * Handle actions within the create.hbs file
	 */

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Save Lecture
		 *
		 * Saves the new lecture record and saves it
		 * in storage.
		 */
		save_lecture: function () {

			/**
			 * Get the input values
			 */
			var title = this.get('lecture_title'), date = this.get('lecture_date');

			/**
			 * Create the lecture record
			 */
			var lecture = this.store.createRecord('lecture', {
				title: title,
				owner: parseInt(sessionStorage.getItem('user_id')),
				date: date
			});

			/**
			 * Store the "this" instance in controller to access within the
			 * "then" function.
			 */
			var controller = this;

			/**
			 * Save the record in storage, 
			 * then refresh the target.router, then transition
			 * to the new lecture's route.
			 */
			lecture.save().then(function() {
				controller.get('target.router').refresh();
				controller.transitionToRoute('lectures.view', lecture);
			});

		}

	}

});